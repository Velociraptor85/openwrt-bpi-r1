## This is the buildsystem for the OpenWrt Linux distribution tailored for the BPI-R1 Router Board (Lamobo-r1).

This build has been tested on Ubuntu 14.10+, Debian Jessie on 32bit and 64bit architecture.

Please use `make menuconfig` to configure your appreciated
configuration for the toolchain and firmware - default config (`def_allconfig` and `def_minconfig`) is supplied for you convenience.

## PLEASE CHANGE the Download directory in the menuconfig to store your downloaded packages!

## Requirements on build system - Linux

Depth information for building: https://wiki.openwrt.org/doc/howto/buildroot.exigence

You need to have installed these Packages (Deb based):
__build-essential flex python python-dev perl unzip gawk kpartx subversion git wget libssl-dev zlib1g-dev libiberty-dev libncurses-dev gettext sudo__

Run `./scripts/feeds update -a` to get all the latest package definitions defined in feeds.conf 
and `./scripts/feeds install -a` to install symlinks of all package/feeds.

Use `make menuconfig` to configure your image.

Simply running `make` will build your firmware or use `./build.sh` to do this process for you.

Specify `multicore` at the end will use all of your cpu cores to speed up the process - `./build.sh multicore`

Speed up build process using multiple hosts (distcc) - `./build.sh multihosts`
Distcc options in the menuconfig - `Jobs per server` `Hosts to use`

It will download all sources, build the cross-compile toolchain, 
the kernel and all choosen applications. This will take a long time to build (1/2 a day for all the packages).
(Recommended to clean your build ever so often - see below)

# Final image and packages are stored in bin/sunxi - Burn the SD image using DD or a DD type GUI.

## To Clean the build
# build.sh
```
./build.sh clean - Deletes contents of the directories /bin and /build_dir.
./build.sh dirclean - Remove the build_dir, staging_dir but keep the config.
```
# OR manually
```
make clean - Deletes contents of the directories /bin and /build_dir. 
make dirclean - Remove the build_dir, staging_dir but keep the config.
make distclean - Full clean EVERYTHING!!
```

The OpenWrt system is documented in docs/. You will need a LaTeX distribution
and the tex4ht package to build the documentation. Type `make -C docs/` to build it.

To build your own firmware you need to have access to a Linux, BSD or MacOSX system
(case-sensitive filesystem required). Cygwin will not be supported because of
the lack of case sensitiveness in the file system.

## Forked from openwrt.org!

# db260179


