#!/bin/bash
# Debug script uncomment set -x
#set -x
cpucount=$(grep -c "processor" /proc/cpuinfo)
cpu=$(($cpucount + 1))

if [ "$1" = "clean" ] ; then
	make clean;exit
fi

if [ "$1" = "dirclean" ] ; then
        make dirclean;exit
fi

if [ "$1" = "" ] ; then
        IGNORE_ERRORS=m,y make V=s 2>&1| tee errors.txt && IGNORE_ERRORS=m,y make target/install V=s 2>&1| tee errors.txt
        [ -f build_*.txt ] && rm build_*.txt
fi

if [ "$1" = "multicore" ] ; then
        IGNORE_ERRORS=m,y make -j$cpu V=s 2>&1| tee errors.txt && IGNORE_ERRORS=m,y make -j$cpu target/install V=s 2>&1| tee errors.txt
        [ -f build_*.txt ] && rm build_*.txt
fi

if [ "$1" = "multihosts" ] ; then
        export IGNORE_ERRORS=m,y
        make V=s 2>&1| tee errors.txt && make target/install V=s 2>&1| tee errors.txt
        [ -f build_*.txt ] && rm build_*.txt
fi

for i in $(grep "failed to build" errors.txt | sed 's/^.*ERROR:[[:space:]]*\([^[:space:]].*\) failed to build.*$/\1/' ) ; do
        if [ "$i" != "" ] ; then
                echo Compiling: ${i}
                make ${i}-compile V=s > build_${i##*/}.txt 2>&1 || echo ${i} : Build failed, see build_${i##*/}.txt
        fi
done
