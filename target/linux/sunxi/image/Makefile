#
# Copyright (C) 2016 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk
include $(INCLUDE_DIR)/image.mk
include $(INCLUDE_DIR)/host.mk

FAT32_BLOCK_SIZE=1024
FAT32_BLOCKS=$(shell echo $$(($(CONFIG_SUNXI_SD_BOOT_PARTSIZE)*1024*1024/$(FAT32_BLOCK_SIZE))))

define Image/BuildKernel
	-mkdir -p $(KDIR_TMP)

	mkimage -A arm -O linux -T kernel -C none \
		-a 0x40008000 -e 0x40008000 \
		-n 'ARM OpenWrt Linux-$(LINUX_VERSION)' \
		-d $(KDIR)/zImage $(BIN_DIR)/$(IMG_PREFIX)-uImage

    ifneq ($(CONFIG_TARGET_ROOTFS_INITRAMFS),)
	$(CP) $(KDIR)/zImage-initramfs $(BIN_DIR)/$(IMG_PREFIX)-zImage-initramfs
	echo -ne '\x00\x00\x00\x00' >> $(BIN_DIR)/$(IMG_PREFIX)-zImage-initramfs
	$(call Image/BuildKernel/MkuImage, \
		none, 0x40008000, 0x40008000, \
		$(BIN_DIR)/$(IMG_PREFIX)-zImage-initramfs, \
		$(BIN_DIR)/$(IMG_PREFIX)-uImage-initramfs \
	)
    endif
endef

define Image/Build/SDCard
	rm -f $(KDIR_TMP)/$(IMG_PREFIX)-$(PROFILE)-boot.img
	rm -rf $(KDIR_TMP)/bootimg
	mkdir $(KDIR_TMP)/bootimg

	# Save boot images for SDcard IMG script
	$(CP) $(KDIR)/uboot-sunxi-$(PROFILE)-boot.scr $(KDIR_TMP)/bootimg/boot.scr
	$(CP) $(DTS_DIR)/$(2).dtb $(KDIR_TMP)/bootimg/dtb
	$(CP) $(BIN_DIR)/$(IMG_PREFIX)-uImage $(KDIR_TMP)/bootimg/uImage
	$(CP) $(KDIR)/uboot-sunxi-$(PROFILE)-u-boot-with-spl.bin $(KDIR_TMP)/bootimg

	# Save root with packages for SDcard IMG script
	$(CP) $(KDIR)/root.$(1) $(KDIR_TMP)/

	# Save root without packages for SDcard IMG script
	$(CP) $(KDIR)/root-sysupgrade.$(1) $(KDIR_TMP)/

	# Make temporary changes by copying script then delete later
	$(CP) openwrt-sdcardimg.sh openwrt-sdcardimg-build.sh

	# Save bin dir location and version name to SDcard IMG script
	sed -i -e 's|basedir=.*|basedir=$(BIN_DIR)/$(IMG_PREFIX)-$(PROFILE)-sdcard|g' openwrt-sdcardimg-build.sh
	sed -i -e 's|version=.*|version=$(IMG_PREFIX)-$(PROFILE)-sdcard|g' openwrt-sdcardimg-build.sh		

	# Save files location to SDcard IMG script
	sed -i -e 's|rootfactory=.*|rootfactory=$(KDIR)/root.$(1)|g' openwrt-sdcardimg-build.sh 
	sed -i -e 's|rootsysupgrade=.*|rootsysupgrade=$(KDIR)/root-sysupgrade.$(1)|g' openwrt-sdcardimg-build.sh 
	sed -i -e 's|bootfiles=.*|bootfiles=$(KDIR_TMP)/bootimg|g' openwrt-sdcardimg-build.sh
	sed -i -e 's|uboot=.*|uboot=uboot-sunxi-$(PROFILE)-u-boot-with-spl.bin|g' openwrt-sdcardimg-build.sh

	# Save boot and root image size to SDcard IMG script
	sed -i -e 's|bootsize=.*|bootsize=$(CONFIG_SUNXI_SD_BOOT_PARTSIZE)|g' openwrt-sdcardimg-build.sh
	sed -i -e 's|rootfactorysize=.*|rootfactorysize=$(CONFIG_TARGET_ROOTFS_PARTSIZE)|g' openwrt-sdcardimg-build.sh
	sed -i -e 's|rootsysupgradesize=.*|rootsysupgradesize=$(CONFIG_TARGET_ROOTFS_SYSUPGPARTSIZE)|g' openwrt-sdcardimg-build.sh

	# Run SDcard IMG script as sudo user
	sudo ./openwrt-sdcardimg-build.sh

	# Remove temp script after build
	rm -f ./openwrt-sdcardimg-build.sh
	

  ifneq ($(CONFIG_TARGET_IMAGES_GZIP),)
	gzip -f9n $(BIN_DIR)/$(IMG_PREFIX)-$(PROFILE)-sdcard/*-factory.img
	gzip -f9n $(BIN_DIR)/$(IMG_PREFIX)-$(PROFILE)-sdcard/*-sysupgrade.img
  endif
endef

define Image/Build/Profile/A10-OLinuXino-Lime
	$(call Image/Build/SDCard,$(1),sun4i-a10-olinuxino-lime)
endef

define Image/Build/Profile/A13-OLinuXino
	$(call Image/Build/SDCard,$(1),sun5i-a13-olinuxino)
endef

define Image/Build/Profile/A20-OLinuXino-Lime
	$(call Image/Build/SDCard,$(1),sun7i-a20-olinuxino-lime)
endef

define Image/Build/Profile/A20-OLinuXino_MICRO
	$(call Image/Build/SDCard,$(1),sun7i-a20-olinuxino-micro)
endef

define Image/Build/Profile/Bananapi
	$(call Image/Build/SDCard,$(1),sun7i-a20-bananapi)
endef

define Image/Build/Profile/Bananapro
	$(call Image/Build/SDCard,$(1),sun7i-a20-bananapro)
endef

define Image/Build/Profile/Lamobo_R1
	$(call Image/Build/SDCard,$(1),sun7i-a20-lamobo-r1)
endef

define Image/Build/Profile/Cubieboard
	$(call Image/Build/SDCard,$(1),sun4i-a10-cubieboard)
endef

define Image/Build/Profile/Cubieboard2
	$(call Image/Build/SDCard,$(1),sun7i-a20-cubieboard2)
endef

define Image/Build/Profile/Cubietruck
	$(call Image/Build/SDCard,$(1),sun7i-a20-cubietruck)
endef

define Image/Build/Profile/OLIMEX_A13_SOM
	$(call Image/Build/SDCard,$(1),sun5i-a13-olinuxino)
endef

define Image/Build/Profile/Mele_M9
	$(call Image/Build/SDCard,$(1),sun6i-a31-hummingbird)
endef

define Image/Build/Profile/Linksprite_pcDuino
	$(call Image/Build/SDCard,$(1),sun4i-a10-pcduino)
endef

define Image/Build/Profile/Linksprite_pcDuino3
	$(call Image/Build/SDCard,$(1),sun7i-a20-pcduino3)
endef

define Image/Build
	$(call Image/Build/$(1),$(1))
	$(call Image/Build/Profile/$(PROFILE),$(1))
endef

$(eval $(call BuildImage))
